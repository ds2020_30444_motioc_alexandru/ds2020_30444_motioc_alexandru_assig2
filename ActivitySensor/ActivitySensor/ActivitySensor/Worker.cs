using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace ActivitySensor
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        //private List<string> _list;
        private string path = @"D:\School\DS2020\JSONParsing\JSONParsing\activity.txt";
        private string[] activityList;
        private int patientId = 0;
        //private string[] info;
        private IModel _channel;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Worker started at: {DateTime.Now}");
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };
            
            IConnection connection = factory.CreateConnection();
            _channel = connection.CreateModel();
            _channel.QueueDeclare("activity-queue",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            //get list from txt
         activityList = File.ReadAllLines(path);
        //_list = new List<string> { "test1", "test2" };

            await base.StartAsync(cancellationToken);
        }
/*
        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Worker stopped at: {DateTime.Now}");
            await base.StopAsync(cancellationToken);
        }
        */

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                foreach(var s in activityList)
                {
                    string[] info = s.Split('\t', '\n', StringSplitOptions.RemoveEmptyEntries);

                    DateTime date1 = Convert.ToDateTime(info[0]);
                    DateTime date2 = Convert.ToDateTime(info[1]);

                    string myActivity = info[2];

                    var patient = new
                    {
                        patient_id = patientId,
                        activity = myActivity,
                        start = date1,
                        end = date2

                    };

                    var patientDataJson = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(patient));
                    patientId++;
                    // publish;
                    //var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(item));
                    _channel.BasicPublish("", "activity-queue", null, patientDataJson); ;
                        
                     await Task.Delay(1000, stoppingToken);
                }

                _logger.LogInformation("all items sent at: {time}", DateTimeOffset.Now);
                await Task.Delay(10000, stoppingToken);

            }
        }
    }
}
