import React, {useState, useEffect} from 'react'
import axios from 'axios';
import { useAuth } from '../context/authcontext';
const CareGiverPage = () => {
  const { id } = useAuth();
    const url = 'https://localhost:44339/Caregiver/' + id +'/Patients'
    const [data, setData] = useState([])
  
    const makeRequest =() => {axios.get(url).then(json => {setData(json.data) ;  console.log(json.data)})}
    useEffect(() => {
        makeRequest();
      }, [])

    return (
        <div className="d-flex flex-row flex-wrap">
          <ul>
            {data.map(pat => {
        return (
          <li> {pat.name} </li>
        )
      })}
          </ul>
        
    </div>
    );
} 

export default CareGiverPage;