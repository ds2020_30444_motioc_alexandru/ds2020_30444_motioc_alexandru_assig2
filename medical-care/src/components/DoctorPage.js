import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useAuth } from "./../context/authcontext";


const querystring = require('querystring');
const CookiesPage = () => {
    const { id } = useAuth(); // id of logged in user
    const url = 'http://localhost:52741/Doctor/' + id ;
    const [data, setData] = useState([])

    
    useEffect(() => {
        axios.get(url).then(json => setData(json.data))
    }, [])

   

    return (
        <React.Fragment>
            <h>Doctor Page</h>
        </React.Fragment>)

}

export default CookiesPage